using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KioutTest.DataModel
{
    /// <summary>
    /// ���������� �� ������� ������
    /// </summary>
    public class StudyGroup
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Id �������� �����, ������� �������� ������
        /// </summary>
        public int CourseId { get; set; }

        /// <summary>
        /// Id �������������, ��������� ���� ������
        /// </summary>
        public int TeacherId { get; set; }

        /// <summary>
        /// ������� ����, ������� �������� ������
        /// </summary>
        public virtual Course Course { get; set; }

        /// <summary>
        /// �������������, �������� ���� ������
        /// </summary>
        public virtual Teacher Teacher { get; set; }

        /// <summary>
        /// ��������, �������� � ������� ������
        /// </summary>
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
