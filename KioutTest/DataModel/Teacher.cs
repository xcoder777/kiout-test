using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KioutTest.DataModel
{
    /// <summary>
    /// ���������� � �������������
    /// </summary>
    public class Teacher
    {
        public int Id { get; set; }

        /// <summary>
        /// ��� �������������
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// ����� ����������� ����� �������������
        /// </summary>
        public string EMail { get; set; }

        /// <summary>
        /// �����������, � ������� ���������� �������������
        /// </summary>
        public virtual ICollection<Organization> Organizations { get; set; }

        /// <summary>
        /// ������� ������, ������� ����� �������������
        /// </summary>
        public virtual ICollection<StudyGroup> StudyGroups { get; set; }
    }
}
