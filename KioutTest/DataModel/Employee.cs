using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KioutTest.DataModel
{
    /// <summary>
    /// ���������� � ���������� �����������
    /// </summary>
    public class Employee
    {
        public int Id { get; set; }

        /// <summary>
        /// ��� ����������
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Id ����������� ����������
        /// </summary>
        public int OrganizationId { get; set; }
        
        /// <summary>
        /// ����������� ����������
        /// </summary>
        public virtual Organization Organization { get; set; }

        /// <summary>
        /// ������� ������, � ������� ������ ���������
        /// </summary>
        public virtual ICollection<StudyGroup> StudyGroups { get; set; }
    }
}
