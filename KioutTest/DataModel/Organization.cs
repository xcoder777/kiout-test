using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KioutTest.DataModel
{
    /// <summary>
    /// ���������� �� �����������
    /// </summary>
    public class Organization
    {
        public int Id { get; set; }

        /// <summary>
        /// �������� �����������
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// ��� �����������
        /// </summary>
        [Required]
        [StringLength(12)]
        public string Inn { get; set; }

        /// <summary>
        /// Id ������������� �� ������������ �������������
        /// </summary>
        public int? TeacherId { get; set; }

        /// <summary>
        /// ���������� �����������
        /// </summary>
        public virtual ICollection<Employee> Employees { get; set; }

        /// <summary>
        /// ������������ �� ������������ �������������
        /// </summary>
        public virtual Teacher Teacher { get; set; }
    }
}
