using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KioutTest.DataModel
{
    /// <summary>
    /// ���������� �� ������� �����
    /// </summary>
    [Table("Courses")]
    public class Course
    {
        public int Id { get; set; }

        /// <summary>
        /// �������� �����
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// ������ ������� �����, ��������� ����
        /// </summary>
        public virtual ICollection<StudyGroup> StudyGroups { get; set; }
    }
}
