﻿using System.Windows.Data;

namespace KioutTest.Converters
{
    /// <summary>
    /// Конвертер bool в текстовую строку для свойства Content пока данные загружаются или сохраняются
    /// </summary>
    [ValueConversion(typeof(bool), typeof(string))]
    public class BoolToTextConverter : BoolToTypeConverterBase<string>
    {
        public BoolToTextConverter() : base("True", "False")
        {
        }
    }
}
