﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace KioutTest.Converters
{
    /// <summary>
    /// Базовый тип для реализации конвертеров из bool
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BoolToTypeConverterBase<T> : IValueConverter
    {
        public BoolToTypeConverterBase(T defaultTrueValue, T defaultFalseValue)
        {
            TrueValue = defaultTrueValue;
            FalseValue = defaultFalseValue;

        }
        public T TrueValue { get; set; }
        public T FalseValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool)) return null;
            return (bool)value ? TrueValue : FalseValue;  
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is T)) return null;

            if (Equals(value, TrueValue)) return true;
            if (Equals(value, FalseValue)) return false;

            return null;
        }
    }
}
