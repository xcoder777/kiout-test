﻿using System.Windows;
using System.Windows.Data;

namespace KioutTest.Converters
{
    /// <summary>
    /// Конвертер bool в Visibility для установки видимости контрола пока данные загружаются или сохраняются
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : BoolToTypeConverterBase<Visibility>
    {
        public BoolToVisibilityConverter()
            : base(Visibility.Visible, Visibility.Hidden)
        {        
        }
    }
}
