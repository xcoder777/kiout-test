﻿using System.Windows.Data;
using System.Windows.Input;

namespace KioutTest.Converters
{
    /// <summary>
    /// Конвертер bool в Cursor для установки курсора пока данные загружаются или сохраняются
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Cursor))]
    public class BoolToCursorConverter : BoolToTypeConverterBase<Cursor>
    {
        public BoolToCursorConverter() : base(Cursors.Arrow, Cursors.Wait)
        {
  
        }
    }
}
