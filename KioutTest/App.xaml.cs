﻿using System.Windows;
using KioutTest.Abstract;
using KioutTest.Concrete;
using KioutTest.ViewModels;
using KioutTest.Windows;

namespace KioutTest
{
    public partial class App
    {
        private KioutTestModel _dataModel;
        private IErrorReporter _errorReporter;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Готовим общие интерфейсы для моделей представления
            _dataModel = new KioutTestModel();
            _errorReporter = new ErrorReporter();

            ViewModelBase baseViewModel = new ViewStudyGroupsViewModel
            {
                ErrorReporter = _errorReporter,
                DataModel = _dataModel
            };

            // Показываем начальную форму
            Current.MainWindow = new ViewStudyGroupsWindow(baseViewModel);
            Current.MainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            // Корректно закрываем контекст на выходе
            if (_dataModel != null)
            {
                _dataModel.Dispose();
                _dataModel = null;
            }
        }

    }
}
