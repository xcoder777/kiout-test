﻿using System.Windows;
using System.Windows.Controls;
using KioutTest.ViewModels;

namespace KioutTest.Windows
{
    /// <summary>
    /// Форма просмотра списка учебных групп
    /// </summary>
    public partial class ViewStudyGroupsWindow
    {
        private readonly ViewStudyGroupsViewModel _viewModel;

        public ViewStudyGroupsWindow(ViewModelBase previousViewModel)
        {
            _viewModel = new ViewStudyGroupsViewModel(previousViewModel);

            InitializeComponent();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = _viewModel;
            await _viewModel.UpdateFromStorageAsync();
        }

        private void ButtonAddStudyGroup_OnClick(object sender, RoutedEventArgs e)
        {
            new CreateStudyGroupWindow(_viewModel).Show();
            Close();
        }

        private async void ButtonRefresh_OnClick(object sender, RoutedEventArgs e)
        {
            await _viewModel.UpdateFromStorageAsync();
        }

        private void ButtonEditStudyGroup_OnClick(object sender, RoutedEventArgs e)
        {
            var item = (StudyGroupViewGridItem)(((Button) e.Source).Tag);

            new EditStudyGroupWindow(item.StudyGroupId, _viewModel).Show();
            Close();
        }
    }
}
