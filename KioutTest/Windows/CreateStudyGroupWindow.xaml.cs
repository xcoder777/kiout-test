﻿using System.Windows;
using System.Windows.Controls;
using KioutTest.ViewModels;

namespace KioutTest.Windows
{
    /// <summary>
    /// Форма создания новой учебной группы
    /// </summary>
    public partial class CreateStudyGroupWindow
    {
        private readonly CreateStudyGroupViewModel _viewModel;

        public CreateStudyGroupWindow(ViewModelBase previousViewModel)
        {
            _viewModel = new CreateStudyGroupViewModel(previousViewModel);
            
            InitializeComponent();
        }

        private async void CreateStudyGroupWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            DataContext = _viewModel;
            await _viewModel.UpdateFromStorageAsync();
        }

        private void ButtonGotoStudyGroupsView_OnClick(object sender, RoutedEventArgs e)
        {
            new ViewStudyGroupsWindow(_viewModel).Show();
            Close();
        }

        private async void ButtonCreateStudyGroup_OnClick(object sender, RoutedEventArgs e)
        {
            var newStudyGroupId = await _viewModel.PerformCreateStudyGroup();
            
            // Если не было ошибки - переходим в форму Редактирование учебной группы
            if (newStudyGroupId != 0)
            {
                new EditStudyGroupWindow(newStudyGroupId, _viewModel).Show();
                Close();
            }
        }

        // Обновляем название учебной группы в модели представления в режиме Explicit, 
        // чтобы всегда иметь актуальное значение из TextBox
        private void TextBoxStudyGroupTitle_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var dependencyProperty = TextBoxStudyGroupTitle.GetBindingExpression(TextBox.TextProperty);
            if (dependencyProperty == null) return;

            // Обновляем модель представления
            dependencyProperty.UpdateSource();
        }


    }
}
