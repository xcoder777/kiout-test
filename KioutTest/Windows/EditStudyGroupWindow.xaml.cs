﻿using System;
using System.Windows;
using System.Windows.Controls;
using KioutTest.ViewModels;

namespace KioutTest.Windows
{
    /// <summary>
    /// Форма для редактирования учебной группы
    /// </summary>
    public partial class EditStudyGroupWindow
    {
        private readonly EditStudyGroupViewModel _viewModel;

        public EditStudyGroupWindow(int studyGroupId, ViewModelBase previousViewModel)
        {
            _viewModel = new EditStudyGroupViewModel(previousViewModel) {StudyGroupId = studyGroupId};

            InitializeComponent();
        }

        private async void EditStudyGroupWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            DataContext = _viewModel;
            await _viewModel.UpdateFromStorageAsync();
        }

        private async void ButtonSaveStudyGroupTitle_OnClick(object sender, RoutedEventArgs e)
        {
            await _viewModel.PerformSaveStudyGroupTitleAsync();
        }

        private void ButtonAddStudent_OnClick(object sender, RoutedEventArgs e)
        {
            new AddStudentToStudyGroupWindow(_viewModel.StudyGroupId, _viewModel).Show();
            Close();
        }

        // Обновляем название учебной группы в модели представления в режиме Explicit, 
        // чтобы всегда иметь актуальное значение из TextBox
        private void TextBoxStudyGroupTitle_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var dependencyProperty = TextBoxStudyGroupTitle.GetBindingExpression(TextBox.TextProperty);
            if (dependencyProperty == null) return;

            // Обновляем модель представления
            dependencyProperty.UpdateSource();
        }

        private async void ButtonDeleteStudentFromStudyGroup_OnClick(object sender, RoutedEventArgs e)
        {
            var item = (StudyGroupStudentViewItem)(((Button)e.Source).Tag);
            if (await _viewModel.PerfomDeleteStudentFromStudyGroup(item.EmployeeId))
            {
                // Обновим содержимое окна в случае успеха
                await _viewModel.UpdateFromStorageAsync();
            }
        }

        private async void ButtonRefresh_OnClick(object sender, RoutedEventArgs e)
        {
            await _viewModel.UpdateFromStorageAsync();
        }

        private void ButtonGotoStudyGroupsView_OnClick(object sender, RoutedEventArgs e)
        {
            new ViewStudyGroupsWindow(_viewModel).Show();
            Close();
        }
    }
}
