﻿using System.Windows;
using KioutTest.ViewModels;

namespace KioutTest.Windows
{
    /// <summary>
    /// Форма добавления студента в учебную группу
    /// </summary>
    public partial class AddStudentToStudyGroupWindow
    {
        private readonly AddStudentToStudyGroupViewModel _viewModel;

        public AddStudentToStudyGroupWindow(int studyGroupId, ViewModelBase previousViewModel)
        {
            _viewModel = new AddStudentToStudyGroupViewModel(previousViewModel) { StudyGroupId = studyGroupId };

            InitializeComponent();
        }

        private async void AddStudentToStudyGroupWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            DataContext = _viewModel;
            await _viewModel.UpdateFromStorageAsync();
        }

        private void ButtonGotoStudyGroupsView_OnClick(object sender, RoutedEventArgs e)
        {
            new ViewStudyGroupsWindow(_viewModel).Show();
            Close();
        }

        private async void ButtonAddStudentToStudyGroup_OnClick(object sender, RoutedEventArgs e)
        {
            if (await _viewModel.PerformAddStudentToStudyGroupAsync())
            {
                // Если не было ошибки - переходим в форму Редактирование учебной группы
                new EditStudyGroupWindow(_viewModel.StudyGroupId, _viewModel).Show();
                Close();
            }
        }
    }
}
