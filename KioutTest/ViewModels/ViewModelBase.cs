using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using KioutTest.Abstract;
using KioutTest.Annotations;

namespace KioutTest.ViewModels
{
    /// <summary>
    /// ������� ����� ��� ���� ������� �������������.
    /// ��������� �������� IsModelViewReady, ������� ���������� ����������� ������ � ������ �������������.
    /// ����� ������������� ������� ��� �������� � ���������� � ������� ������ ����� �������� ErrorReporter,
    /// ��� �� ������ ��������� ����������� ������ ������ (�������� DataModel).
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// ����������� ��-���������
        /// </summary>
        protected ViewModelBase()
        {
        }

        /// <summary>
        /// ����������� ��� �������� � ������� ������ �������������
        /// ��������������� �������� ���������� ������ �������������
        /// </summary>
        /// <param name="previousViewModel">���������� ������ �������������</param>
        protected ViewModelBase(ViewModelBase previousViewModel)
        {
            ErrorReporter = previousViewModel.ErrorReporter;
            DataModel = previousViewModel.DataModel;
        }

        private bool _isViewModelReady;


        /// <summary>
        /// ������ ������
        /// </summary>
        public IKioutDataModel DataModel { get; set; }

        /// <summary>
        /// ����� ��� ������ ��������� �� �������.
        /// </summary>
        public IErrorReporter ErrorReporter { get; set; }

        /// <summary>
        /// ���� ���������� ������ � ������.
        /// ������������ �� ����� �������� � ���������� ������ �������������
        /// </summary>
        public bool IsViewModelReady
        {
            get { return _isViewModelReady; }
            private set
            {
                if (value == _isViewModelReady) return;
                _isViewModelReady = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// ��������� �������� ������ ������������� �� ������ � ������� ������ ����� ErrorReporter (���� �� �� null).
        /// � ������ ������ �������� ������������� ������ ������ ������� SetEmpty() � ���������� false.
        /// </summary>
        /// <param name="loadAction">�������� �� �������� ������ ������������� �� DataModel</param>
        /// <returns>���������� false, ���� ��������� ������ � ����������� ������ ������.</returns>
        protected async Task<bool> LoadFromDataModel(Func<Task> loadAction)
        {
            // �������� ������� ����������� ������ ������
            // ��� ����������� ������, ������� ����������� �� ������
            if (DataModel == null) throw new InvalidOperationException("DataModel not set");

            try
            {
                // ������� ���� ���������� ������
                IsViewModelReady = false;

                // ��������� ������
                await loadAction();

                return true;
            }
            catch (Exception exc)
            {
                // ��������� �� ������, ���� ���� ����
                if (ErrorReporter != null)
                    ErrorReporter.ReportException("������ �������� ������:", exc);

                // ������������� ������ ������
                SetEmpty();
                return false;
            }
            finally
            {
                // ���������� ���� ���������� ������
                IsViewModelReady = true;
            }
        }

        /// <summary>
        /// ��������� ������ ������ ������������� �� ��������� � ��������� ������ (����� IErrorReporter).
        /// � ������ ������ ��������������� ������ ������ (������� SetEmpty)
        /// </summary>
        public async Task UpdateFromStorageAsync()
        {
            await LoadFromDataModel(DoLoadFromStorageAsync);
        }

        /// <summary>
        /// ��������� ��������������� �������� ������ ������ ������������� �� ���������.
        /// </summary>
        /// <returns></returns>
        protected abstract Task DoLoadFromStorageAsync();

        /// <summary>
        /// ������������� ������ ������ ������������� �� ������ ������.
        /// </summary>
        protected abstract void SetEmpty();


        /// <summary>
        /// ��������� ��������� ������ ������ DataModel �� ������ ������� ������ �������������.
        /// � ������ ������ ������� �� � ������� ErrorReporter (���� �� �� null) � ���������� �������� ��-��������� (0 ��� int, false ��� bool)
        /// </summary>
        /// <param name="modifyAction">�������� �� ��������� ������ DataModel �� ������ ������� ������ �������������</param>
        /// <returns>���������� Id ����� �������� ��� 0 � ������ ������</returns>
        protected async Task<TResult> ModifyDataModel<TResult>(Func<Task<TResult>> modifyAction)
        {
            // �������� ������� ����������� ������ ������
            // ��� ����������� ������, ������� ����������� �� ������
            if (DataModel == null) throw new InvalidOperationException("DataModel not set");

            try
            {
                // ���������� ���� ���������� ������
                IsViewModelReady = false;

                // ��������� ����������� �������� �� ���������
                // ������ ������ �� ������ ������� ������ �������������
                return await modifyAction();
            }
            catch (Exception exc)
            {
                // ������� ������ ���� ���������
                if (ErrorReporter != null)
                    ErrorReporter.ReportException("������ ��� ���������� ������:", exc);

                // ���������� �������� ��-��������� (0 ��� int, false ��� bool)
                return default(TResult);
            }
            finally
            {
                // �������������  ���� ���������� ������
                IsViewModelReady = true;
            }
        }

        #region INotifyPropertyChanged implementaion

        public virtual event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}