﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using KioutTest.DataModel;

namespace KioutTest.ViewModels
{
    /// <summary>
    /// Модель представления для создания новой учебной группы
    /// </summary>
    public class CreateStudyGroupViewModel : ViewModelBase
    {
        private string _studyGroupTitle;
        private int _courseId;
        private List<Course> _courses = new List<Course>();
        private int _teacherId;
        private List<Teacher> _teachers = new List<Teacher>();
        private bool _isReadyToCreateStudyGroup;

        /// <summary>
        /// Конструктор по-умолчанию
        /// </summary>
        public CreateStudyGroupViewModel()
        {
        }

        /// <summary>
        /// Конструктор для переноса в текущую модель представления
        /// фундаментальных настроек предыдущей модели представления
        /// </summary>
        /// <param name="previousViewModel">Предыдущая модель представления</param>
        public CreateStudyGroupViewModel(ViewModelBase previousViewModel)
            :base(previousViewModel)
        {
        }

        /// <summary>
        /// Список доступных преподавателей
        /// </summary>
        public List<Teacher> Teachers
        {
            get { return _teachers; }
            set
            {
                if (Equals(value, _teachers)) return;
                _teachers = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Id выбранного для учебной группы преподавателя
        /// </summary>
        public int TeacherId
        {
            get { return _teacherId; }
            set
            {
                _teacherId = value;
                
                // Выполним проверку на достаточность данных для создания новой группы
                CheckReadyToCreateStudyGroup();
            }
        }

        /// <summary>
        /// Список доступных названий курсов
        /// </summary>
        public List<Course> Courses
        {
            get { return _courses; }
            set
            {
                if (Equals(value, _courses)) return;
                _courses = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Выбранный Id курса
        /// </summary>
        public int CourseId
        {
            get { return _courseId; }
            set
            {
                _courseId = value;

                // Выполним проверку на достаточность данных для создания новой группы
                CheckReadyToCreateStudyGroup();
            }
        }

        /// <summary>
        /// Название новой группы
        /// </summary>
        public string StudyGroupTitle
        {
            get { return _studyGroupTitle; }
            set
            {
                if (value == _studyGroupTitle) return;
                _studyGroupTitle = value;
                OnPropertyChanged();

                // Выполним проверку на достаточность данных для создания новой группы
                CheckReadyToCreateStudyGroup();
            }
        }

        /// <summary>
        /// Флаг, что установлены все необходимые данные для создания новой учебной группы.
        /// </summary>
        public bool IsReadyToCreateStudyGroup
        {
            get { return _isReadyToCreateStudyGroup; }
            set
            {
                if (value == _isReadyToCreateStudyGroup) return;
                _isReadyToCreateStudyGroup = value;
                OnPropertyChanged();
            }
        }

        // Проверяет, достаточно ли данных для создания новой учебной группы
        private void CheckReadyToCreateStudyGroup()
        {
            IsReadyToCreateStudyGroup = IsViewModelReady &&
                                        CourseId > 0 &&
                                        TeacherId > 0 &&
                                        !string.IsNullOrWhiteSpace(StudyGroupTitle);
        }

        /// <summary>
        /// Выполняет непосредственно загрузку данных модели представления из хранилища.
        /// </summary>
        /// <returns></returns>
        protected override Task DoLoadFromStorageAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                Courses = DataModel.Courses.AsNoTracking().ToList();
                CourseId = 0;
                Teachers = DataModel.Teachers.AsNoTracking().ToList();
                TeacherId = 0;
                StudyGroupTitle = null;
            });
        }

        /// <summary>
        /// Устанавливает пустую модель представления на случай ошибки.
        /// </summary>
        protected override void SetEmpty()
        {
            StudyGroupTitle = null;
            Courses = new List<Course>();
            CourseId = 0;
            Teachers = new List<Teacher>();
            TeacherId = 0;
        }

        /// <summary>
        /// Выполняет создание новой учебной группы с выводом ошибки через ErrorReporter (если установлен)
        /// </summary>
        /// <returns>Возвращает Id новой группы или 0 в случае ошибки</returns>
        public Task<int> PerformCreateStudyGroup()
        {
            return ModifyDataModel(DoCreateStudyGroup);
        }

        // Действия по созданию новой учебной группы
        // Возвращает Id новой группы
        private Task<int> DoCreateStudyGroup()
        {
            return Task<int>.Factory.StartNew(() =>
            {
                // Создаем новую учебную группу
                var newStudyGroup = new StudyGroup
                {
                    Course = DataModel.Courses.Single(c => c.Id == CourseId),
                    Teacher = DataModel.Teachers.Single(t => t.Id == TeacherId),
                    Title = StudyGroupTitle,
                    Employees = new List<Employee>()
                };

                // Добавляем ее в модель
                DataModel.StudyGroups.Add(newStudyGroup);

                // Сохраняем изменения в модели данных в хранилище
                DataModel.SaveChanges();

                return newStudyGroup.Id;
            });
        }
    }
}
