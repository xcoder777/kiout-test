﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KioutTest.ViewModels
{
    /// <summary>
    /// Модель представления для редактирования учебной группы
    /// </summary>
    public class EditStudyGroupViewModel : ViewModelBase
    {
        
        private bool _isStudyGroupNameChanged;
        private string _studyGroupTitle;
        private string _teacherName;
        private List<StudyGroupStudentViewItem> _studyGroupStudents = new List<StudyGroupStudentViewItem>();

        /// <summary>
        /// Конструктор по-умолчанию
        /// </summary>
        public EditStudyGroupViewModel()
        {
        }

        /// <summary>
        /// Конструктор для переноса в текущую модель представления
        /// фундаментальных настроек предыдущей модели представления
        /// </summary>
        /// <param name="previousViewModel">Предыдущая модель представления</param>
        public EditStudyGroupViewModel(ViewModelBase previousViewModel)
            :base(previousViewModel)
        {
        }

        /// <summary>
        /// Id учебной группы
        /// </summary>
        public int StudyGroupId { get; set; }

        /// <summary>
        /// Имя преподавателя учебной группы
        /// </summary>
        public string TeacherName
        {
            get { return _teacherName; }
            set
            {
                if (value == _teacherName) return;
                _teacherName = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Название учебной группы
        /// </summary>
        public string StudyGroupTitle
        {
            get { return _studyGroupTitle; }
            set
            {
                if (value == _studyGroupTitle) return;
                _studyGroupTitle = value;
                OnPropertyChanged();

                // Выставим флаг, что были изменения названия учебной группы
                IsStudyGroupNameChanged = true;
            }
        }

        /// <summary>
        /// Признак изменения имени учебной группы
        /// </summary>
        public bool IsStudyGroupNameChanged
        {
            get { return _isStudyGroupNameChanged; }
            set
            {
                if (value == _isStudyGroupNameChanged) return;
                _isStudyGroupNameChanged = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Список студентов, приписанных к учебной группе
        /// </summary>
        public List<StudyGroupStudentViewItem> StudyGroupStudents
        {
            get { return _studyGroupStudents; }
            set
            {
                if (Equals(value, _studyGroupStudents)) return;
                _studyGroupStudents = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Выполняет непосредственно загрузку данных модели представления из хранилища.
        /// </summary>
        /// <returns></returns>
        protected override Task DoLoadFromStorageAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                var studyGroup = DataModel.StudyGroups.AsNoTracking().Single(sg => sg.Id == StudyGroupId);

                StudyGroupTitle = studyGroup.Title;
                TeacherName = studyGroup.Teacher.Name;
                IsStudyGroupNameChanged = false;
                StudyGroupStudents = studyGroup.Employees
                    .Select(employee =>
                        new StudyGroupStudentViewItem
                        {
                            EmployeeId = employee.Id,
                            EmployeeName = employee.Name,
                            OrganizationTitle = employee.Organization.Title
                        })
                    .ToList();
            });
        }

        /// <summary>
        /// Устанавливает пустую модель представления на случай ошибки.
        /// </summary>
        protected override void SetEmpty()
        {
            TeacherName = null;
            StudyGroupTitle = null;
            IsStudyGroupNameChanged = false;
            StudyGroupStudents = new List<StudyGroupStudentViewItem>();
        }

        /// <summary>
        /// Сохраняет название учебной группы с выводом ошибок через ErrorReporter (если установлен)
        /// </summary>
        /// <returns>Возвращает false в случае ошибки</returns>
        public Task<bool> PerformSaveStudyGroupTitleAsync()
        {
            return ModifyDataModel(DoSaveStudyGroupTitleAsync);
        }

        // Сохраняет название учебной группы в DataModel
        private Task<bool> DoSaveStudyGroupTitleAsync()
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                var studyGroup = DataModel.StudyGroups.Single(sg => sg.Id == StudyGroupId);
                studyGroup.Title = StudyGroupTitle;

                // Сохраняем изменения в DataModel
                DataModel.SaveChanges();

                // Помечаем, что сохранено
                IsStudyGroupNameChanged = false;

                // Всегда возвращаем true, т.к. false означает ошибку
                return true;
            });
        }

        /// <summary>
        /// Удаляет студента из учебной группы с выводом ошибок через ErrorReporter (если установлен)
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public Task<bool> PerfomDeleteStudentFromStudyGroup(int employeeId)
        {
            return ModifyDataModel(() => DoDeleteStudentFromStudyGroup(employeeId));
        }

        // Удаляет студента из учебной группы 
        private Task<bool> DoDeleteStudentFromStudyGroup(int employeeId)
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                var studyGroup = DataModel.StudyGroups.Single(sg => sg.Id == StudyGroupId);
                var employee = studyGroup.Employees.Single(e => e.Id == employeeId);

                studyGroup.Employees.Remove(employee);

                // Сохраняем изменения в DataModel
                DataModel.SaveChanges();

                // Всегда возвращаем true, т.к. false означает ошибку
                return true;
            });
        }
    }

    /// <summary>
    /// Информация о студенте учебной группы
    /// </summary>
    public class StudyGroupStudentViewItem
    {
        /// <summary>
        /// Id сотрудника
        /// </summary>
        public int EmployeeId { get; set; }

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string EmployeeName { get; set; }

        /// <summary>
        /// Название организации сотрудника
        /// </summary>
        public string OrganizationTitle { get; set; }
    }
}
