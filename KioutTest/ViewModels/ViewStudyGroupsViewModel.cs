﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace KioutTest.ViewModels
{
    /// <summary>
    /// Модель для формы отображения списка имеющихся учетных групп
    /// </summary>
    public class ViewStudyGroupsViewModel : ViewModelBase
    {
        private List<StudyGroupViewGridItem> _data = new List<StudyGroupViewGridItem>();

        /// <summary>
        /// Конструктор по-умолчанию
        /// </summary>
        public ViewStudyGroupsViewModel()
        {
        }

        /// <summary>
        /// Конструктор для переноса в текущую модель представления
        /// фундаментальных настроек предыдущей модели представления
        /// </summary>
        /// <param name="previousViewModel">Предыдущая модель представления</param>
        public ViewStudyGroupsViewModel(ViewModelBase previousViewModel)
            :base(previousViewModel)
        {
        }

        /// <summary>
        /// Список учебных групп для отображения
        /// </summary>
        public List<StudyGroupViewGridItem> Data
        {
            get { return _data; }
            set
            {
                if (Equals(value, _data)) return;
                _data = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Выполняет непосредственно загрузку данных модели представления из хранилища.
        /// </summary>
        /// <returns></returns>
        protected override Task DoLoadFromStorageAsync()
        {
            return Task.Factory.StartNew(() =>
            {
                Data = DataModel.StudyGroups
                    .AsNoTracking()
                    .Select(studyGroup => new StudyGroupViewGridItem
                    {
                        Title = studyGroup.Title,
                        TeacherName = studyGroup.Teacher.Name,
                        StudentCount = studyGroup.Employees.Count(),
                        StudyGroupId = studyGroup.Id
                    })
                    .ToList();
            });
        }

        /// <summary>
        /// Устанавливает пустую модель представления на случай ошибки.
        /// </summary>
        protected override void SetEmpty()
        {
            Data = new List<StudyGroupViewGridItem>();
        }
    }

    /// <summary>
    /// Информация об учебной группе
    /// </summary>
    public class StudyGroupViewGridItem
    {
        /// <summary>
        /// Id учебной группы
        /// </summary>
        public int StudyGroupId { get; set; }
        
        /// <summary>
        /// Название группы
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// ФИО Преподавателя
        /// </summary>
        public string TeacherName { get; set; }

        /// <summary>
        /// Количество студентов в группе
        /// </summary>
        public int StudentCount { get; set; }
    }
}
