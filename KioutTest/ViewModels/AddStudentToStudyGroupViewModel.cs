﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KioutTest.DataModel;

namespace KioutTest.ViewModels
{
    /// <summary>
    /// Модель представления для добавления студента в учебную группу
    /// </summary>
    public class AddStudentToStudyGroupViewModel : ViewModelBase
    {
        private bool _isReadyToAddStudentToStudyGroup;
        private int _organizationId;
        private int _employeeId;
        private string _studyGroupTitle;
        private string _teacherName;
        private List<Organization> _organizations = new List<Organization>();
        private List<Employee> _organizationEmployees = new List<Employee>();

        /// <summary>
        /// Конструктор по-умолчанию
        /// </summary>
        public AddStudentToStudyGroupViewModel()
        { }

        /// <summary>
        /// Конструктор для переноса в текущую модель представления
        /// фундаментальных настроек предыдущей модели представления
        /// </summary>
        /// <param name="previousViewModel">Предыдущая модель представления</param>
        public AddStudentToStudyGroupViewModel(ViewModelBase previousViewModel)
            : base(previousViewModel)
        {
        }

        /// <summary>
        /// Id учебной группы
        /// </summary>
        public int StudyGroupId { get; set; }

        /// <summary>
        /// Название  учебной группы
        /// </summary>
        public string StudyGroupTitle
        {
            get { return _studyGroupTitle; }
            set
            {
                if (value == _studyGroupTitle) return;
                _studyGroupTitle = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Имя преподавателя учебной группы
        /// </summary>
        public string TeacherName
        {
            get { return _teacherName; }
            set
            {
                if (value == _teacherName) return;
                _teacherName = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Список организаций преподавателя
        /// </summary>
        public List<Organization> Organizations
        {
            get { return _organizations; }
            set
            {
                if (Equals(value, _organizations)) return;
                _organizations = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Id выбранной организации
        /// </summary>
        public int OrganizationId
        {
            get { return _organizationId; }
            set
            {
                _organizationId = value;
                LoadOrganizationEmployees();
            }
        }

        /// <summary>
        /// Список сотрудников организации с OrganizationId, которые 
        /// не входят в учебную группу
        /// </summary>
        public List<Employee> OrganizationEmployees
        {
            get { return _organizationEmployees; }
            set
            {
                if (Equals(value, _organizationEmployees)) return;
                _organizationEmployees = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Id выбранного сотрудника для включения в учебную группу
        /// </summary>
        public int EmployeeId
        {
            get { return _employeeId; }
            set
            {
                _employeeId = value;
                CheckReadyToAddStudentToStudyGroup();
            }
        }

        // Проверить достаточно ли информации для выполнения добавления студента в учебную группу
        private void CheckReadyToAddStudentToStudyGroup()
        {
            // Должен быть выбран конкретный сотрудник
            IsReadyToAddStudentToStudyGroup = EmployeeId > 0;
        }

        /// <summary>
        /// Признак, что вся необходимая информация для добавления студента в учебную группу установлена.
        /// </summary>
        public bool IsReadyToAddStudentToStudyGroup
        {
            get { return _isReadyToAddStudentToStudyGroup; }
            set
            {
                if (value == _isReadyToAddStudentToStudyGroup) return;
                _isReadyToAddStudentToStudyGroup = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Выполняет непосредственно загрузку данных модели представления из хранилища.
        /// </summary>
        /// <returns></returns>
        protected override Task DoLoadFromStorageAsync()
        {            
            return Task.Factory.StartNew(() =>
                {
                    var studyGroup = DataModel.StudyGroups.Single(sg => sg.Id == StudyGroupId);

                    StudyGroupTitle = studyGroup.Title;
                    TeacherName = studyGroup.Teacher.Name;
                    Organizations = studyGroup.Teacher.Organizations.ToList();

                    var organizationId = OrganizationId > 0 ? OrganizationId : 0;
                    if (organizationId > 0)
                    {
                        OrganizationEmployees = studyGroup.Teacher.Organizations
                            .Where(o => o.Id == organizationId)
                            .SelectMany(o => o.Employees)
                            .Except(studyGroup.Employees)
                            .ToList();
                    }
                    else
                    {
                        OrganizationEmployees = new List<Employee>();
                    }
                });
        }

        // Устанавливает пустую модель
        protected override void SetEmpty()
        {
            Organizations = new List<Organization>();
            OrganizationEmployees = new List<Employee>();
            OrganizationId = 0;
            EmployeeId = 0;
            StudyGroupTitle = null;
            TeacherName = null;
        }

        // Загрузить сотрудников для выбранной организации
        private async void LoadOrganizationEmployees()
        {
            await UpdateFromStorageAsync();
        }

        /// <summary>
        /// Выполняет добавление студента в учебную группу с контролем ошибок
        /// и выводом сообщения через ErrorReporter (если установлен)
        /// </summary>
        /// <returns></returns>
        public Task<bool> PerformAddStudentToStudyGroupAsync()
        {
            return ModifyDataModel(DoAddStudentToStudyGroupAsync);
        }

        // Выполняет добавление студента в учебную группу
        protected Task<bool> DoAddStudentToStudyGroupAsync()
        {
            return Task<bool>.Factory.StartNew(() =>
            {
                var studyGroup = DataModel.StudyGroups.Single(sg => sg.Id == StudyGroupId);
                var employee = DataModel.Employees.Single(e => e.Id == EmployeeId);

                studyGroup.Employees.Add(employee);

                // Сохраняем изменения в хранилище модели данных
                DataModel.SaveChanges();
                
                return true;
            });
        }

    }
}
