﻿using System.Data.Entity;
using KioutTest.DataModel;

namespace KioutTest.Concrete
{
    /// <summary>
    /// Реализация стратегии создания БД для модели данных и наполнения ее тестовыми данными
    /// </summary>
    public class KioutTestDatabaseInitializer : CreateDatabaseIfNotExists<KioutTestModel>
    {
        // Заполнение БД тестовыми данными
        protected override void Seed(KioutTestModel model)
        {
            model.Courses.Add(new Course {Title = "Курс 1"});
            model.Courses.Add(new Course {Title = "Курс 2"});
            model.Courses.Add(new Course {Title = "Курс 3"});

            var t1 = new Teacher { Name = "Воробьев В.В.", EMail = "teacher1@mail.com" };
            var t2 = new Teacher { Name = "Орлов О.О.", EMail = "teacher2@mail.com" };
            var t3 = new Teacher { Name = "Снегов С.С.", EMail = "teacher3@mail.com" };

            model.Teachers.Add(t1);
            model.Teachers.Add(t2);
            model.Teachers.Add(t3);

            var o1 = new Organization {Title = "РЖД", Inn = "1234567890", Teacher = t1};
            var o2 = new Organization {Title = "Аэрофлот", Inn = "0987654321", Teacher = t2};
            var o3 = new Organization { Title = "ООО \"Ромашка\"", Inn = "5432109876", Teacher = t1};
            var o4 = new Organization { Title = "ООО \"Лютик\"", Inn = "6789012345", Teacher = t2};
            var o5 = new Organization { Title = "ООО \"Синеглазка\"", Inn = "2134567890", Teacher = t3};

            model.Organizations.Add(o1);
            model.Organizations.Add(o2);
            model.Organizations.Add(o3);
            model.Organizations.Add(o4);
            model.Organizations.Add(o5);
            
            model.Employees.Add(new Employee {Name = "Карасев К.К.", Organization = o1});
            model.Employees.Add(new Employee {Name = "Семенов С.С.", Organization = o1});
            model.Employees.Add(new Employee {Name = "Светлов С.А.", Organization = o1});
            model.Employees.Add(new Employee {Name = "Иванова С.А.", Organization = o1});

            model.Employees.Add(new Employee {Name = "Окунев О.О.", Organization = o2});
            model.Employees.Add(new Employee {Name = "Петров П.П.", Organization = o2});
            model.Employees.Add(new Employee {Name = "Гречко Б.Б.", Organization = o2});
            model.Employees.Add(new Employee {Name = "Петрова Н.А.", Organization = o2});
            
            model.Employees.Add(new Employee {Name = "Андреев А.А.", Organization = o3});
            model.Employees.Add(new Employee {Name = "Большов Б.Б.", Organization = o3});

            model.Employees.Add(new Employee { Name = "Величко В.В.", Organization = o4 });
            model.Employees.Add(new Employee { Name = "Григорова Н.П.", Organization = o4 });

            model.Employees.Add(new Employee { Name = "Дмитрова Д.Д.", Organization = o5 });

            model.SaveChanges();

        }
    }
}
