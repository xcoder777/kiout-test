﻿using System;
using System.Windows;
using KioutTest.Abstract;

namespace KioutTest.Concrete
{
    /// <summary>
    /// Простейшая реализация регистратора ошибок.
    /// Выводит сообщение на экран
    /// </summary>
    public class ErrorReporter : IErrorReporter
    {
        public void ReportException(string message, Exception exc)
        {
            MessageBox.Show(string.Format("{0}\n{1}", message, exc.Message), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
