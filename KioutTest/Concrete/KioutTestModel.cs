using System.Data.Entity;
using KioutTest.Abstract;
using KioutTest.DataModel;

namespace KioutTest.Concrete
{
    /// <summary>
    /// ���������� ������ ������ � �������������� EntityFramework.
    /// ��� ���������� ������������ �� ������� �� � ��������� ��������� �������.
    /// ��������� ����������� � SQL Server ���������� � ����� App.config ��� ������ KioutTestModel.
    /// 
    /// ��� ������ � ������������ �� ������������ ������ ����� ����� ������ � ������ � �� 
    /// (Database role: db_datareader + db_datawriter ��� db_owner)
    /// ��� ����������� ������� �� � ������ ���������� ������ ����� ����� ��������� �� (Server role: dbcreator)
    /// </summary>
    public class KioutTestModel : DbContext, IKioutDataModel
    {
        public KioutTestModel()
            : base("name=KioutTestModel")
        {
            // ������� �� ���� �� ��� � ��������� ��������� �������
            Database.SetInitializer((new KioutTestDatabaseInitializer()));
        }

        public virtual IDbSet<Course> Courses { get; set; }
        public virtual IDbSet<Employee> Employees { get; set; }
        public virtual IDbSet<Organization> Organizations { get; set; }
        public virtual IDbSet<StudyGroup> StudyGroups { get; set; }
        public virtual IDbSet<Teacher> Teachers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>()
                .HasMany(e => e.StudyGroups)
                .WithRequired(e => e.Course)
                .HasForeignKey(e => e.CourseId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.StudyGroups)
                .WithMany(e => e.Employees)
                .Map(m => m.ToTable("EmployeeStudyGroup").MapLeftKey("EmployeeId").MapRightKey("StudyGroupId"));

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Organization)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Teacher>()
                .HasMany(e => e.StudyGroups)
                .WithRequired(e => e.Teacher)
                .WillCascadeOnDelete(false);
        }
    }
}
