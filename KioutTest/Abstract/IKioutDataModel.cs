﻿using System.Data.Entity;
using KioutTest.DataModel;

namespace KioutTest.Abstract
{
    /// <summary>
    /// Интерфейс модели данных
    /// </summary>
    public interface IKioutDataModel
    {
        /// <summary>
        /// Курсы
        /// </summary>
        IDbSet<Course> Courses { get; set; }

        /// <summary>
        /// Сотрудники
        /// </summary>
        IDbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Организации
        /// </summary>
        IDbSet<Organization> Organizations { get; set; }

        /// <summary>
        /// Учбные группы
        /// </summary>
        IDbSet<StudyGroup> StudyGroups { get; set; }

        /// <summary>
        /// Преподаватели
        /// </summary>
        IDbSet<Teacher> Teachers { get; set; }

        /// <summary>
        /// Сохранить изменения в модели данных в хранилище
        /// </summary>
        /// <returns></returns>
        int SaveChanges();
    }
}