﻿using System;

namespace KioutTest.Abstract
{
    /// <summary>
    /// Интерфейс для сообщения об ошибках
    /// </summary>
    public interface IErrorReporter
    {
        /// <summary>
        /// Сообщить текст возникшего исключения
        /// </summary>
        /// <param name="message">Текстовое описание ошибки</param>
        /// <param name="exc">Возникшее исключение</param>
        void ReportException(string message, Exception exc);
    }
}
